module.exports = {
    'Shows home page correctly'(client) {
      client
        .url('http://localhost:3000/')
        .waitForElementVisible('header')
        .assert.elementPresent('.App')
        .assert.elementPresent('.wel input')
        .assert.elementPresent('.welcom')
      client.end();
    },

    'Check that changing list color works'(browser) {
      browser
        .url('http://localhost:3000/shop')
        .assert.urlContains('shop')
        .waitForElementVisible('.card.h-100')
        .assert.containsText("#color-change", "Change to dark")
        .click("#color-change")
        .assert.containsText("#color-change", "Change to light")
        .assert.cssClassPresent(".card.h-100", "black");
      browser.end();
    },

    'Add one item to cart'(browser) {
      browser
        .url('http://localhost:3000/shop')
        .assert.urlContains('shop')
        .waitForElementVisible('.card.h-100')
        .click(".card.h-100:nth-of-type(1) .add-to-cart")
        .click('.navbar li:nth-of-type(2)')
        .elements('css selector','.item-desc1', function(result) {
          browser
            .assert.equal(result.value.length, 1, 'Should have only 1 item')
            .assert.attributeContains('.quantImput', 'value', "1")
        });
        ;
      browser.end();
    },
    'add more than one item to cart'(browser) {
      browser
      .url('http://localhost:3000/shop')
      .assert.urlContains('shop')
        .waitForElementVisible('.card.h-100')
        .click(".card.h-100:nth-of-type(1) .plus")
        .click(".card.h-100:nth-of-type(1) .add-to-cart")
        .click('.navbar li:nth-of-type(2)')
        .elements('css selector','.item-desc1', function(result) { //naidi vse div s takim klassom, i zapishi ih v massiv value, kotorõi naoditsa vnutri resulta
          browser
            .assert.equal(result.value.length, 1, 'Should have 1 items')
            .assert.attributeContains('.quantImput', 'value', "2")
        });
        ;
      browser.end();
    },
    'edit item quantity in a cart'(browser) {
      browser
      .url('http://localhost:3000/shop')
      .assert.urlContains('shop')
      .waitForElementVisible('.card.h-100')
      .click(".card.h-100:nth-of-type(1) .add-to-cart")
      .click('.navbar li:nth-of-type(2)')
      .waitForElementVisible('.item-desc1')
      .elements('css selector','.item-desc1', function(result) {
        browser
          .assert.equal(result.value.length, 1, 'Should have only 1 item')
          .assert.attributeContains('.quantImput', 'value', "1")
      })
      .click(".item-desc1:nth-of-type(1) .minus")
      .elements('css selector','.item-desc1', function(result) {
        browser
          .assert.equal(result.value.length, 1, 'Should have 1 item')
          .assert.attributeContains('.quantImput', 'value', "0")
      });
      ;
    browser.end();
  },
  'delete item in a cart'(browser) {
    browser
      .url('http://localhost:3000/shop')
      .assert.urlContains('shop')
      .waitForElementVisible('.card.h-100')
      .click(".card.h-100:nth-of-type(1) .add-to-cart")
      .click('.navbar li:nth-of-type(2)')
      .elements('css selector','.item-desc1', function(result) {
        browser
          .assert.equal(result.value.length, 1, 'Should have only 1 item')
          .assert.attributeContains('.quantImput', 'value', "1")
      })
      .click(".item-desc1:nth-of-type(1) .delete")
      .expect.element('.item-desc1').to.not.be.present;
    browser.end();
  },
  'test grid button'(browser) {
    browser
      .url('http://localhost:3000/shop')
      .assert.urlContains('shop')
      .waitForElementVisible('.card.h-100')
      .click(".grid.btn")
      .assert.cssClassPresent(".col-lg-4.col-md-6.mb-4");
    browser.end();
  },
  'test list button'(browser) {
    browser
      .url('http://localhost:3000/shop')
      .assert.urlContains('shop')
      .waitForElementVisible('.card.h-100')
      .click(".list.btn")
      .assert.elementPresent('.col-lg-12.mb-4')
      .click(".grid.btn")
      .assert.elementPresent('.col-lg-4.col-md-6.mb-4');
    browser.end();
  },
  'add 2 items to cart, check if total is right' (browser) {
    browser
      .url('http://localhost:3000/shop')
      .waitForElementVisible('.col-lg-4.col-md-6.mb-4:nth-of-type(1)')
      .click(".col-lg-4.col-md-6.mb-4:nth-of-type(1) .add-to-cart")
      .click(".col-lg-4.col-md-6.mb-4:nth-of-type(2) .add-to-cart")
      .click('.navbar li:nth-of-type(2)')
      .elements('css selector','.total', function(result) {
        browser
          .assert.equal(result.value.length, 1, 'Should have only 1 total')
          .assert.containsText('.total', "101.99")
      })
      browser.end();
    },
}

