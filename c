[1mdiff --git a/src/App.css b/src/App.css[m
[1mindex 096ad87..5724814 100644[m
[1m--- a/src/App.css[m
[1m+++ b/src/App.css[m
[36m@@ -6,10 +6,9 @@[m
 }[m
 .purchase-card {[m
   border-style: solid;[m
[31m-  border-color: rgb(129, 129, 195);[m
[31m-}[m
[31m-.purchase-card{[m
[32m+[m[32m  border-color: rgb(131, 131, 216);[m
   width: 350px;[m
[32m+[m[32m  height: 180px;[m
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);[m
   padding: len;[m
 }[m
[36m@@ -329,6 +328,7 @@[m [mfont-size: 2rem;[m
 }[m
 .purchase-card-black {[m
   width: 350px;[m
[32m+[m[32m  height: 180px;[m
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);[m
   padding: len;[m
 }[m
[36m@@ -373,4 +373,12 @@[m [mfont-size: 2rem;[m
 .welcom {[m
   color: white;[m
   text-align: center;[m
[32m+[m[32m}[m
[32m+[m[32m.superTotal {[m
[32m+[m[32m  width: 350px ;[m
[32m+[m[32m  margin: auto;[m
[32m+[m[32m  text-align: center;[m
[32m+[m[32m  background-color: white;[m
[32m+[m[32m  border-style: solid;[m
[32m+[m[32m  border-color: rgb(129, 129, 195);[m
 }[m
\ No newline at end of file[m
[1mdiff --git a/src/App.tsx b/src/App.tsx[m
[1mindex 8c3f7dd..b8eaa9c 100644[m
[1m--- a/src/App.tsx[m
[1m+++ b/src/App.tsx[m
[36m@@ -82,11 +82,12 @@[m [mclass App extends React.Component<AppProps, AppState> {[m
     return ([m
       <div>[m
         <div className="cover">[m
[31m-          <header><li className={homeActiveOrNot}>[m
[31m-            <h3><Link to={'/'} onClick={() => this.activeLink('home')}>Home</Link>[m
[31m-            </h3></li>[m
[32m+[m[32m          <header>[m
             <Navbar>[m
               <ul>[m
[32m+[m[32m              <li className={homeActiveOrNot}>[m
[32m+[m[32m            <Link to={'/'} onClick={() => this.activeLink('home')}>Home</Link>[m
[32m+[m[32m            </li>[m
                 <li className={ShopActiveOrNot}>[m
                   <Link to={'/shop'} onClick={() => this.activeLink('shop')}>Shop</Link>[m
                 </li>[m
[1mdiff --git a/src/components/EstimatedTotal/EstimatedTotal.tsx b/src/components/EstimatedTotal/EstimatedTotal.tsx[m
[1mindex 239b90d..bf76104 100644[m
[1m--- a/src/components/EstimatedTotal/EstimatedTotal.tsx[m
[1m+++ b/src/components/EstimatedTotal/EstimatedTotal.tsx[m
[36m@@ -14,8 +14,8 @@[m [mclass EstimatedTotal extends React.Component<EstimatedTotalProps, EstimatedTotal[m
     render() {[m
         return ([m
             <Row>[m
[31m-                <Col  md={6}><h2>Est. Total</h2></Col>[m
[31m-                <Col className="total" md={6}><h2>{`$${this.props.totalSum}`}</h2></Col>[m
[32m+[m[32m                <Col  md={6}><h3>Price:</h3></Col>[m
[32m+[m[32m                <Col className="total" md={6}><h3>{`$${this.props.totalSum.toFixed(2)}`}</h3></Col>[m
             </Row>[m
         );[m
     }[m
[36m@@ -23,6 +23,7 @@[m [mclass EstimatedTotal extends React.Component<EstimatedTotalProps, EstimatedTotal[m
 const mapStateToProps = (state: any) => ({[m
     addedItems: state.addedItems,[m
     total: state.total[m
[32m+[m[41m    [m
 });[m
 [m
 const mapDispatchToProps = (dispatch: any) => ({[m
[1mdiff --git a/src/components/Shipping.tsx b/src/components/Shipping.tsx[m
[1mindex 1cf708b..3533e8e 100644[m
[1m--- a/src/components/Shipping.tsx[m
[1m+++ b/src/components/Shipping.tsx[m
[36m@@ -20,8 +20,8 @@[m [mclass Shipping extends React.Component<ShippingProps, ShippingState> {[m
     render() {[m
          return ([m
             <Row>[m
[31m-            <Col md={6}><h2>Shipping</h2></Col>[m
[31m-            <Col md={6}><h2>{`$${this.props.totalShipping}`}</h2></Col>[m
[32m+[m[32m            <Col md={6}><h3>Shipping:</h3></Col>[m
[32m+[m[32m            <Col md={6}><h3>{`$${this.props.totalShipping.toFixed(2)}`}</h3></Col>[m
         </Row>[m
         );[m
     }[m
[1mdiff --git a/src/components/ShoppingCart.tsx b/src/components/ShoppingCart.tsx[m
[1mindex 41d51db..9d24a33 100644[m
[1m--- a/src/components/ShoppingCart.tsx[m
[1m+++ b/src/components/ShoppingCart.tsx[m
[36m@@ -9,7 +9,7 @@[m [mimport EstimatedTotal from '../components/EstimatedTotal/EstimatedTotal';[m
 // import PromoCodeDiscount from '../components/PromoCode/PromoCode';[m
 import { Button } from 'react-bootstrap';[m
 import { removeItem, updateItem } from '../actions/Cart.actions.';[m
[31m-import { Row } from 'react-bootstrap';[m
[32m+[m[32mimport { Row, Col } from 'react-bootstrap';[m
 import { Link } from 'react-router-dom';[m
 import { connect } from 'react-redux';[m
 import { singleAddedItem } from '../reducers/cartReducer';[m
[36m@@ -32,6 +32,7 @@[m [minterface ShoppingCartProps {[m
     HandleRemoveItem: (itemID: number, quantity: number) => void;[m
     HandleUpdateItem: (itemID: number, quantity: number) => void;[m
     EstimatedTotal: number;[m
[32m+[m[32m    totalShipping: number;[m
 }[m
 [m
 class ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState> {[m
[36m@@ -124,6 +125,7 @@[m [mclass ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState>[m
     //     }[m
     // }[m
     render() {[m
[32m+[m[32m        let superTotal = this.props.EstimatedTotal + this.props.totalShipping;[m
         let Change = 'purchase-card';[m
         if (this.state.isCardBacgroundWhite2 === false) {[m
             Change = 'purchase-card-black';[m
[36m@@ -175,27 +177,27 @@[m [mclass ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState>[m
             );[m
         });[m
         return ([m
[31m-            <div className="container">[m
[32m+[m[32m            <div className={`container ${Change}`}>[m
                 <h3>Your order</h3>[m
[31m-                <Row>{[m
[31m-                    addedItems.length ? // if[m
[31m-                        addedItems[m
[31m-                        : <p>Nothing.</p>[m
[31m-                }[m
[32m+[m[32m                <Row>[m
[32m+[m[32m                    {[m
[32m+[m[32m                        addedItems.length ? // if[m
[32m+[m[32m                            addedItems[m
[32m+[m[32m                            : <p>Nothing.</p>[m
[32m+[m[32m                    }[m
                     {/* //addedItems if true, nothing if false */}[m
                 </Row>[m
                 <div>[m
                     <Button onClick={this.cardColourChange}>{ChangeText}</Button>[m
[31m-                    <Container className={Change}>[m
[32m+[m[32m                    <Container>[m
                         {/* <SubTotal price={this.state.total.toFixed(2) viewFunction={this.listwiew}} />[m
                         <PickupSavings price={this.state.PickupSavings.toString()} />[m
                         <TaxesFees taxes={this.state.TaxesFees.toFixed(2)} /> */}[m
                         <hr />[m
[31m-                        <Shipping />[m
[31m-                        <hr />[m
                         <EstimatedTotal totalSum={this.props.EstimatedTotal} />[m
                         {/* <ItemDetails price={this.props.EstimatedTotal.toFixed(2)} /> */}[m
                         <hr />[m
[32m+[m[32m                        <Shipping />[m
                         {/* <PromoCodeDiscount[m
                             giveDiscount={this.giveDiscountHandler}[m
                             isDisabled={this.state.disablePromoButton}[m
[36m@@ -203,7 +205,15 @@[m [mclass ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState>[m
                         /> */}[m
                         <hr />[m
                     </Container>[m
[32m+[m[32m                    <Col className="superTotal"> <h3> Total: ${superTotal}</h3> </Col>[m
                 </div>[m
[32m+[m[32m                <hr />[m
[32m+[m[32m                <hr />[m
[32m+[m[32m                <hr />[m
[32m+[m[32m                <hr />[m
[32m+[m[32m                <Button className="return" variant="info"> Return to shop</Button>[m
[32m+[m[32m                <hr />[m
[32m+[m[32m                <Button className="pay" variant="info"> Pay for Your order </Button>[m
             </div>[m
         );[m
     }[m
[36m@@ -211,7 +221,8 @@[m [mclass ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState>[m
 }[m
 const mapStateToProps = (state: any) => ({[m
     items: state.cart.addedItems,[m
[31m-    EstimatedTotal: state.cart.total[m
[32m+[m[32m    EstimatedTotal: state.cart.total,[m
[32m+[m[32m    totalShipping: state.cart.totalShipping[m
 });[m
 [m
 const mapDispatchToProps = (dispatch: any) => ({[m
[1mdiff --git a/tests/tests.js b/tests/tests.js[m
[1mindex 32fdabb..46ca1bc 100644[m
[1m--- a/tests/tests.js[m
[1m+++ b/tests/tests.js[m
[36m@@ -1,115 +1,115 @@[m
 module.exports = {[m
[31m-  //   'Shows home page correctly'(client) {[m
[31m-  //     client[m
[31m-  //       .url('http://localhost:3000/')[m
[31m-  //       .waitForElementVisible('header')[m
[31m-  //       .assert.elementPresent('.App')[m
[31m-  //       .assert.elementPresent('.wel input')[m
[31m-  //       .assert.elementPresent('.welcom')[m
[31m-  //     client.end();[m
[31m-  //   },[m
[32m+[m[32m    'Shows home page correctly'(client) {[m
[32m+[m[32m      client[m
[32m+[m[32m        .url('http://localhost:3000/')[m
[32m+[m[32m        .waitForElementVisible('header')[m
[32m+[m[32m        .assert.elementPresent('.App')[m
[32m+[m[32m        .assert.elementPresent('.wel input')[m
[32m+[m[32m        .assert.elementPresent('.welcom')[m
[32m+[m[32m      client.end();[m
[32m+[m[32m    },[m
 [m
[31m-  //   'Check that changing list color works'(browser) {[m
[31m-  //     browser[m
[31m-  //       .url('http://localhost:3000/shop')[m
[31m-  //       .assert.urlContains('shop')[m
[31m-  //       .waitForElementVisible('.card.h-100')[m
[31m-  //       .assert.containsText("#color-change", "Change to dark")[m
[31m-  //       .click("#color-change")[m
[31m-  //       .assert.containsText("#color-change", "Change to light")[m
[31m-  //       .assert.cssClassPresent(".card.h-100", "black");[m
[31m-  //     browser.end();[m
[31m-  //   },[m
[32m+[m[32m    'Check that changing list color works'(browser) {[m
[32m+[m[32m      browser[m
[32m+[m[32m        .url('http://localhost:3000/shop')[m
[32m+[m[32m        .assert.urlContains('shop')[m
[32m+[m[32m        .waitForElementVisible('.card.h-100')[m
[32m+[m[32m        .assert.containsText("#color-change", "Change to dark")[m
[32m+[m[32m        .click("#color-change")[m
[32m+[m[32m        .assert.containsText("#color-change", "Change to light")[m
[32m+[m[32m        .assert.cssClassPresent(".card.h-100", "black");[m
[32m+[m[32m      browser.end();[m
[32m+[m[32m    },[m
 [m
[31m-  //   'Add one item to cart'(browser) {[m
[31m-  //     browser[m
[31m-  //       .url('http://localhost:3000/shop')[m
[31m-  //       .assert.urlContains('shop')[m
[31m-  //       .waitForElementVisible('.card.h-100')[m
[31m-  //       .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[31m-  //       .click('.navbar li:nth-of-type(2)')[m
[31m-  //       .elements('css selector','.item-desc1', function(result) {[m
[31m-  //         browser[m
[31m-  //           .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[31m-  //           .assert.attributeContains('.quantImput', 'value', "1")[m
[31m-  //       });[m
[31m-  //       ;[m
[31m-  //     browser.end();[m
[31m-  //   },[m
[31m-  //   'add more than one item to cart'(browser) {[m
[31m-  //     browser[m
[31m-  //     .url('http://localhost:3000/shop')[m
[31m-  //     .assert.urlContains('shop')[m
[31m-  //       .waitForElementVisible('.card.h-100')[m
[31m-  //       .click(".card.h-100:nth-of-type(1) .plus")[m
[31m-  //       .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[31m-  //       .click('.navbar li:nth-of-type(2)')[m
[31m-  //       .elements('css selector','.item-desc1', function(result) { //naidi vse div s takim klassom, i zapishi ih v massiv value, kotorõi naoditsa vnutri resulta[m
[31m-  //         browser[m
[31m-  //           .assert.equal(result.value.length, 1, 'Should have 1 items')[m
[31m-  //           .assert.attributeContains('.quantImput', 'value', "2")[m
[31m-  //       });[m
[31m-  //       ;[m
[31m-  //     browser.end();[m
[31m-  //   },[m
[31m-  //   'edit item quantity in a cart'(browser) {[m
[31m-  //     browser[m
[31m-  //     .url('http://localhost:3000/shop')[m
[31m-  //     .assert.urlContains('shop')[m
[31m-  //     .waitForElementVisible('.card.h-100')[m
[31m-  //     .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[31m-  //     .click('.navbar li:nth-of-type(2)')[m
[31m-  //     .waitForElementVisible('.item-desc1')[m
[31m-  //     .elements('css selector','.item-desc1', function(result) {[m
[31m-  //       browser[m
[31m-  //         .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[31m-  //         .assert.attributeContains('.quantImput', 'value', "1")[m
[31m-  //     })[m
[31m-  //     .click(".item-desc1:nth-of-type(1) .minus")[m
[31m-  //     .elements('css selector','.item-desc1', function(result) {[m
[31m-  //       browser[m
[31m-  //         .assert.equal(result.value.length, 1, 'Should have 1 item')[m
[31m-  //         .assert.attributeContains('.quantImput', 'value', "0")[m
[31m-  //     });[m
[31m-  //     ;[m
[31m-  //   browser.end();[m
[31m-  // },[m
[31m-  // 'delete item in a cart'(browser) {[m
[31m-  //   browser[m
[31m-  //     .url('http://localhost:3000/shop')[m
[31m-  //     .assert.urlContains('shop')[m
[31m-  //     .waitForElementVisible('.card.h-100')[m
[31m-  //     .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[31m-  //     .click('.navbar li:nth-of-type(2)')[m
[31m-  //     .elements('css selector','.item-desc1', function(result) {[m
[31m-  //       browser[m
[31m-  //         .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[31m-  //         .assert.attributeContains('.quantImput', 'value', "1")[m
[31m-  //     })[m
[31m-  //     .click(".item-desc1:nth-of-type(1) .delete")[m
[31m-  //     .expect.element('.item-desc1').to.not.be.present;[m
[31m-  //   browser.end();[m
[31m-  // },[m
[31m-  // 'test grid button'(browser) {[m
[31m-  //   browser[m
[31m-  //     .url('http://localhost:3000/shop')[m
[31m-  //     .assert.urlContains('shop')[m
[31m-  //     .waitForElementVisible('.card.h-100')[m
[31m-  //     .click(".grid.btn")[m
[31m-  //     .assert.cssClassPresent(".col-lg-4.col-md-6.mb-4");[m
[31m-  //   browser.end();[m
[31m-  // },[m
[31m-  // 'test list button'(browser) {[m
[31m-  //   browser[m
[31m-  //     .url('http://localhost:3000/shop')[m
[31m-  //     .assert.urlContains('shop')[m
[31m-  //     .waitForElementVisible('.card.h-100')[m
[31m-  //     .click(".list.btn")[m
[31m-  //     .assert.elementPresent('.col-lg-12.mb-4')[m
[31m-  //     .click(".grid.btn")[m
[31m-  //     .assert.elementPresent('.col-lg-4.col-md-6.mb-4');[m
[31m-  //   browser.end();[m
[31m-  // },[m
[32m+[m[32m    'Add one item to cart'(browser) {[m
[32m+[m[32m      browser[m
[32m+[m[32m        .url('http://localhost:3000/shop')[m
[32m+[m[32m        .assert.urlContains('shop')[m
[32m+[m[32m        .waitForElementVisible('.card.h-100')[m
[32m+[m[32m        .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[32m+[m[32m        .click('.navbar li:nth-of-type(2)')[m
[32m+[m[32m        .elements('css selector','.item-desc1', function(result) {[m
[32m+[m[32m          browser[m
[32m+[m[32m            .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[32m+[m[32m            .assert.attributeContains('.quantImput', 'value', "1")[m
[32m+[m[32m        });[m
[32m+[m[32m        ;[m
[32m+[m[32m      browser.end();[m
[32m+[m[32m    },[m
[32m+[m[32m    'add more than one item to cart'(browser) {[m
[32m+[m[32m      browser[m
[32m+[m[32m      .url('http://localhost:3000/shop')[m
[32m+[m[32m      .assert.urlContains('shop')[m
[32m+[m[32m        .waitForElementVisible('.card.h-100')[m
[32m+[m[32m        .click(".card.h-100:nth-of-type(1) .plus")[m
[32m+[m[32m        .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[32m+[m[32m        .click('.navbar li:nth-of-type(2)')[m
[32m+[m[32m        .elements('css selector','.item-desc1', function(result) { //naidi vse div s takim klassom, i zapishi ih v massiv value, kotorõi naoditsa vnutri resulta[m
[32m+[m[32m          browser[m
[32m+[m[32m            .assert.equal(result.value.length, 1, 'Should have 1 items')[m
[32m+[m[32m            .assert.attributeContains('.quantImput', 'value', "2")[m
[32m+[m[32m        });[m
[32m+[m[32m        ;[m
[32m+[m[32m      browser.end();[m
[32m+[m[32m    },[m
[32m+[m[32m    'edit item quantity in a cart'(browser) {[m
[32m+[m[32m      browser[m
[32m+[m[32m      .url('http://localhost:3000/shop')[m
[32m+[m[32m      .assert.urlContains('shop')[m
[32m+[m[32m      .waitForElementVisible('.card.h-100')[m
[32m+[m[32m      .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[32m+[m[32m      .click('.navbar li:nth-of-type(2)')[m
[32m+[m[32m      .waitForElementVisible('.item-desc1')[m
[32m+[m[32m      .elements('css selector','.item-desc1', function(result) {[m
[32m+[m[32m        browser[m
[32m+[m[32m          .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[32m+[m[32m          .assert.attributeContains('.quantImput', 'value', "1")[m
[32m+[m[32m      })[m
[32m+[m[32m      .click(".item-desc1:nth-of-type(1) .minus")[m
[32m+[m[32m      .elements('css selector','.item-desc1', function(result) {[m
[32m+[m[32m        browser[m
[32m+[m[32m          .assert.equal(result.value.length, 1, 'Should have 1 item')[m
[32m+[m[32m          .assert.attributeContains('.quantImput', 'value', "0")[m
[32m+[m[32m      });[m
[32m+[m[32m      ;[m
[32m+[m[32m    browser.end();[m
[32m+[m[32m  },[m
[32m+[m[32m  'delete item in a cart'(browser) {[m
[32m+[m[32m    browser[m
[32m+[m[32m      .url('http://localhost:3000/shop')[m
[32m+[m[32m      .assert.urlContains('shop')[m
[32m+[m[32m      .waitForElementVisible('.card.h-100')[m
[32m+[m[32m      .click(".card.h-100:nth-of-type(1) .add-to-cart")[m
[32m+[m[32m      .click('.navbar li:nth-of-type(2)')[m
[32m+[m[32m      .elements('css selector','.item-desc1', function(result) {[m
[32m+[m[32m        browser[m
[32m+[m[32m          .assert.equal(result.value.length, 1, 'Should have only 1 item')[m
[32m+[m[32m          .assert.attributeContains('.quantImput', 'value', "1")[m
[32m+[m[32m      })[m
[32m+[m[32m      .click(".item-desc1:nth-of-type(1) .delete")[m
[32m+[m[32m      .expect.element('.item-desc1').to.not.be.present;[m
[32m+[m[32m    browser.end();[m
[32m+[m[32m  },[m
[32m+[m[32m  'test grid button'(browser) {[m
[32m+[m[32m    browser[m
[32m+[m[32m      .url('http://localhost:3000/shop')[m
[32m+[m[32m      .assert.urlContains('shop')[m
[32m+[m[32m      .waitForElementVisible('.card.h-100')[m
[32m+[m[32m      .click(".grid.btn")[m
[32m+[m[32m      .assert.cssClassPresent(".col-lg-4.col-md-6.mb-4");[m
[32m+[m[32m    browser.end();[m
[32m+[m[32m  },[m
[32m+[m[32m  'test list button'(browser) {[m
[32m+[m[32m    browser[m
[32m+[m[32m      .url('http://localhost:3000/shop')[m
[32m+[m[32m      .assert.urlContains('shop')[m
[32m+[m[32m      .waitForElementVisible('.card.h-100')[m
[32m+[m[32m      .click(".list.btn")[m
[32m+[m[32m      .assert.elementPresent('.col-lg-12.mb-4')[m
[32m+[m[32m      .click(".grid.btn")[m
[32m+[m[32m      .assert.elementPresent('.col-lg-4.col-md-6.mb-4');[m
[32m+[m[32m    browser.end();[m
[32m+[m[32m  },[m
   'add 2 items to cart, check if total is right' (browser) {[m
     browser[m
       .url('http://localhost:3000/shop')[m
