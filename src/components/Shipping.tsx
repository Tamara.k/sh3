import * as React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';


interface ShippingState {
  }
interface ShippingProps {
   totalShipping: number;
}

class Shipping extends React.Component<ShippingProps, ShippingState> {
    constructor (props: ShippingProps) {
        super(props);
        this.state = {
        };
    }
    
    
    render() {
         return (
            <Row>
            <Col md={6}><h3>Shipping:</h3></Col>
            <Col md={6}><h3>{`$${this.props.totalShipping.toFixed(2)}`}</h3></Col>
        </Row>
        );
    }
}
const mapStateToProps = (state: any) => ({
    totalShipping: state.cart.totalShipping,
   
});

const mapDispatchToProps = (dispatch: any) => ({
    
});
export default connect(mapStateToProps, mapDispatchToProps)(Shipping);