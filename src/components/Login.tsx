import * as React from 'react';
import { Form, Button } from 'react-bootstrap';
import '../App.css';

interface LoginProps {

}
interface LoginState {

}

class Login extends React.Component<LoginProps, LoginState> {
    constructor(props: LoginProps) {
        super(props);

        this.state = {

        };
    }


    handleSubmit = (event: any) => {
        event.preventDefault();
        const data = new FormData(event.target);
        fetch('http://localhost:3030/login', {
            method: 'POST',
            body: data,
        });
    }


    // const { id } = this.props.match.params;
    // fetch(`http://localhost:3030/users/${id}`)
    //     .then(response => response.json())
    //     .then((data) => {
    //         this.setState({ LocalUser: data });
    //         console.log(this.state);
    //     });


    render() {
        return (

            <Form className={'container'} onSubmit={this.handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control name="email" type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control name="password" type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group controlId="formBasicChecbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <Button variant="primary" type="submit" >
                    Submit
                </Button>
            </Form>

            //  <Form className="loginForm">
            //  <div className="form-group">
            //  <label>Email address</label>
            //  <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
            //  <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
            // </div>
            // <div className="form-group">
            // <label>Password</label>
            // <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
            // </div>
            // <div className="form-check">
            // <input type="checkbox" className="form-check-input" id="exampleCheck1" />
            // <label>Check me out</label>
            // </div>
            // <Button type="submit" className="btn btn-primary">Submit</Button>
            // </Form> 
        );
    }
}
export default Login;