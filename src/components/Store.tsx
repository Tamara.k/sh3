import '../App.css';
import * as React from 'react';
import { connect } from 'react-redux';
import pic1 from '../assets/1.jpg';
import pic2 from '../assets/2.jpg';
import pic3 from '../assets/3.jpg';
import { Carousel } from 'react-bootstrap';
import { Button, Row } from 'react-bootstrap';
// import { InputGroup } from 'react-bootstrap';
import { handleOnUserListStyleClick } from '../actions/userPreferencesAction';
import { addToCar } from '../actions/Cart.actions.';
import { singleAddedItem } from '../reducers/cartReducer';
// import ItemDetails from './ItemDetails/ItemDetails';
// import ItemDetails from './ItemDetails/ItemDetails';
// import NumberPicker from 'react-number-picker';
import {Link} from 'react-router-dom';

interface HomeState {
  total: number;
  PickupSavings: number;
  TaxesFees: number;
  EstimatedTotal: number;
  disablePromoButton: boolean;
  taxes: number;
  // howUserSeesItems: string;
  howUserSeesCards: string;
  isCardBacgroundWhite: boolean;
  value: number;
  prevState: number;
  localItems: singleAddedItem[];

}
interface HomeProps {
  listStyle: string;
  items: any;
  handleOnUserListStyleClick: (styleName: string) => void;
  addToCarFunction: (itemID: number, quantity: number) => void;
}
// ///function discountPrice(item: any) {
//   return (<strong className="price-strike"> <p><b>Price: {item.price}$</b></p></strong>
//   <div className="discounted-price"><p><b>Discounted Price: {item['discounted-price']}$</b></p></div>);
// }

// //function fullPrice(item: any) {
//   return ( <p><b>Price: {item.price}$</b></p>);
// }

class Store extends React.Component<HomeProps, HomeState> {
  constructor(props: HomeProps) {
    super(props);

    this.state = {
      total: 100,
      PickupSavings: -3.85,
      TaxesFees: 0,
      EstimatedTotal: 0,
      disablePromoButton: false,
      taxes: 5,
      // howUserSeesItems: 'grid',
      howUserSeesCards: 'white',
      isCardBacgroundWhite: true,
      value: 0,
      prevState: 0,
      localItems: [],
    };
    // this.onHandlChange = this.onHandlChange.bind(this); ne nuzno dlja arrowfunction
  }
  componentDidMount() {
    const localItems = this.props.items.map((item: any) => Object.assign(item, { quantity: 1 })); //1r, stobõ postavitj defaultnõe znacenija
    this.setState({ localItems });  //võzõvaetsa vsegda 1 raz, polucil items massiv iz redux stora i zapisõvaem v lokalni state, stobõ ego menjat,
  }

  componentDidUpdate(prevProps: HomeProps) {
    const items = this.props.items;
    const localItems = this.state.localItems;
    if (localItems.length === 0 && items.length > 0) {
      const localItems = items.map((item: any) => Object.assign(item, { quantity: 1 })); //1r, stobõ postavitj defaultnõe znacenija
      this.setState({ localItems });  //võzõvaetsa vsegda 1 raz, polucil items massiv iz redux stora i zapisõvaem v lokalni state, stobõ ego menjat,
    }

  
  }

  plusQuantity = (itemID: number) => {
    // const { localItems } = this.state; // destructuring
    const localItems = this.state.localItems;   //iz state massiv localItems i zapisõvaem v peremennuju localItems stobõ ee mozno bõlo menjatj
    const index = localItems.findIndex(item => item.itemID === itemID);  //item.itemID -iz massiva a (imja ne pridumõvaem)  === itemID eto nazvanie peremennoi iz samoi funkcii plusQuantity.
    let clickedItem = localItems[index];
    clickedItem.quantity += 1;

    localItems[index] = clickedItem; //raznüe veshi s stro4koi 73
    this.setState({ localItems });  //izmenili massiv i obnovljajem ego v state
  }

  minusQuantity = (itemID: number) => {
    const { localItems } = this.state; // destructuring
    // const localItems = this.state.localItems;
    const index = localItems.findIndex(item => item.itemID === itemID); //v localitems ishem item u kotoroge ego ItemId raven tomu; sto prihodit v funkciju kak parametr.
    let clickedItem = localItems[index];
    if (clickedItem.quantity >= 1) {
      clickedItem.quantity -= 1;
    }

    localItems[index] = clickedItem;
    this.setState({ localItems });
  }

  //  quantFunc = (itemID: number) => {
  //   let substractedQuantity = minusFunc(localItems.find(item: any => item.itemID === itemID.quantity)){
  //     return itemID.quantity - 1; 
  //   }

  // let addedQuantity = localItems.find(item: any => item.quantity === plusFunc());

  // plusFunc = (itemID: number) => {
  //  this.setState({
  //   quantity: this.state.quantity + 1
  //  });
  //  }

  //   handleChange(value: any) {
  //     console.log('new value',value);
  //     this.setState({NpValue: value});
  // }

  listView = () => {
    if (this.props.listStyle === 'grid') {
      this.props.handleOnUserListStyleClick('list');
    }
  }
  gridView = () => {
    if (this.props.listStyle === 'list') {
      this.props.handleOnUserListStyleClick('grid');
    }
  }
  cardColourChange = () => {
    this.setState({
      isCardBacgroundWhite: !this.state.isCardBacgroundWhite
    });
  }
  // whiteView =() => {
  //   if(this.state.howUserSeesCards ==='black'){this.setState({ howUserSeesCards: 'white'});}
  // }
  // blackView =() => {
  //   if(this.state.howUserSeesCards ==='white'){this.setState({ howUserSeesCards: 'black'});}
  // }
  onHandlChange = (event: any, itemID: number) => {
    if (isNaN(event.target.value)) {
      alert('Write number!');
    } else {
      const localItems = this.state.localItems;
      const index = localItems.findIndex(item => item.itemID === itemID);
      let clickedItem = localItems[index];
      clickedItem.quantity = event.target.value;
      localItems[index] = clickedItem;
      this.setState({ localItems });
    }
  }
  // StorageFunc = (item: any) => {
    
  //     this.props.addToCarFunction(item.itemID, item.quantity);
  //      localStorage.setItem(item.itemID, item.quantity);
    
  // }

  render() {
    console.log(this.state.isCardBacgroundWhite);
    let ChangeText = 'Change to dark';
    if (this.state.isCardBacgroundWhite === false) {
      ChangeText = 'Change to light';
    }
    let shopItems = this.state.localItems.map((item: any) => {
      let realPriceClass = '';
      if (item['discounted-price'] !== undefined) {
        realPriceClass = 'price-strike';
      }

      let discountedPriceElement = <div className='discount-price'><p>
        <b>Discounted Price: {item['discounted-price']}$</b></p></div>;
      if (item['discounted-price'] === undefined) {
        discountedPriceElement = <span />;
      }
      let itemsListClassName = 'col-lg-4 col-md-6 mb-4';
      if (this.props.listStyle === 'list') {
        itemsListClassName = 'col-lg-12 mb-4';
      }
      // let cardColourChanges = 'card h-100';
      // if (this.state.howUserSeesCards === 'black'){
      //   cardColourChanges = 'card h-100 black';

      // }
      let Change = 'card h-100';
      if (this.state.isCardBacgroundWhite === false) {
        Change = 'card h-100 black';
      }
      return (
        <div className={itemsListClassName} key={item.name}>
          <div className={Change}>
          <Link className="ItemAllLink" to={{pathname: `/shop/${item.itemID}`, state: {fromStore: true}}}>
            <div className="card-image">
              <img src={item.image} alt={item.name} />
              <span className="card-title">{item.name}</span>
              <span className="btn-floating halfway-fab waves-effect waves-light red" />
            </div>
            <div className="card-content">
              <p className="trimmed-description">{item.description}</p>
              <strong className={realPriceClass}>
                <div className="price"> <p><b>Price: {item.price}$</b></p></div>
              </strong>
              {/* //skobki stobõ pokazattj znacenie peremennoi v html */}
              <div>{discountedPriceElement}</div>
            </div></Link><span className="addButton">
              <Button 
                variant="info"
                onClick={() => this.props.addToCarFunction(item.itemID, item.quantity)}
                className="add-to-cart"
              >
                Add
              </Button>
              <Button className="minus" variant="info" onClick={() => this.minusQuantity(item.itemID)}> - </Button>
              <input type="text" className="quantImput" onChange={event => this.onHandlChange(event, item.itemID)} value={item.quantity} />
              <Button className="plus" variant="info" onClick={() => this.plusQuantity(item.itemID)}> + </Button>
            </span>
            
            {/* <NumberPicker value={this.state.NpValue}  onChange={() => this.handleChange(item.NpVaue)} />;  */}
            {/* <div className="quantity-input"> */}
            {/* <Button className="quantity-input__modifier quantity-input__modifier--left" onClick={() => this.decrement(item.quantity)}>
                &mdash;
        </Button>
              {/* <InputGroup className="quantity-input__screen" type="text" value={this.state.value} readonly /> */}
            {/* <Button className="quantity-input__modifier quantity-input__modifier--right" onClick={() => this.increment(item.quantity)}>
                &#xff0b;
        </Button> */}
          </div>
        </div>
      );
    }
    );

    let amountOfItems = shopItems.length;
    return (
      <React.Fragment>
        <div className="container">
          <h3 className="center">Our items ( {amountOfItems} )  </h3>
          <div className="col-lg-3 hide">
            <div className="list-group">
              <a href="#" className="list-group-item">Chairs</a>
              <a href="#" className="list-group-item">Wall Art</a>
              <a href="#" className="list-group-item">Lamps</a>
            </div>
          </div>
          <Carousel className='hide'>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={pic1}
                alt="First slide"
              />
              <Carousel.Caption>
                <h3>Have nice  day</h3>
                <p>Shop for home decorations.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={pic2}
                alt="Third slide"
              />
              <Carousel.Caption>
                <h3>Bye something</h3>
                <p>Promo compaign.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={pic3}
                alt="Third slide"
              />
              <Carousel.Caption>
                <h3>Shop is open</h3>
                <p>Find your dream chair.</p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
          <div className="row box">
            <div id="container2">
              <Row>
                <div id="btnContainer">
                  <Button className="list btn" onClick={this.listView}> List </Button>
                  <Button className="grid btn" onClick={this.gridView}>Grid</Button>
                </div>
                <div id="btnContainer">
                  {/* <Button className="list.btn" onClick={this.whiteView}> White </Button>
                  <Button className="grid.btn" onClick={this.blackView}> Black</Button> */}
                  <Button id="color-change" onClick={this.cardColourChange}>{ChangeText}</Button>
                </div>
              </Row>
              <Row>{shopItems}</Row>
            </div>
          </div>
          <p className="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: any) => ({
  items: state.cart.items,
  listStyle: state.userPreferences.listStyle
});

const mapDispatchToProps = (dispatch: any) => ({
  handleOnUserListStyleClick: (style: string) => dispatch(handleOnUserListStyleClick(style)),
  addToCarFunction: (itemID: number, quantity: number) => dispatch(addToCar(itemID, quantity)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Store);