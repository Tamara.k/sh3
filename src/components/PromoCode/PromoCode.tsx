import * as React from 'react';
import { Button, Collapse, Form, Row, Col, Card, FormGroup, FormControl, FormLabel } from 'react-bootstrap';
import { connect } from 'react-redux';
import handleChange from '../../actions/promoCodeActions';

interface PromoCodeDiscountState {
    open: boolean;
    value: string;
}
interface PromoCodeDiscountProps {
    promoCodeString: any;
    giveDiscount: () => void;
    isDisabled: boolean;
    handleChange: () => void; 
}

class PromoCodeDiscount extends React.Component<PromoCodeDiscountProps, PromoCodeDiscountState> {
    constructor(props: PromoCodeDiscountProps) {
        super(props);

        this.state = {
            open: false,
            value: ''

        };
    }
    onButtonClick = () => {
        this.setState({ open: !this.state.open });
    }
    // handleChange = (e: any) => {
    //     this.props.handleChange(e);
    // }
    render() {
        return (
            <div>
                <Button
                    className="promo-code-button"
                    variant="link"
                    onClick={this.onButtonClick}
                >
                    {this.state.open === false ? `Apply ` : `Hide `}
                    promo code
                    {this.state.open === false ? ` +` : ` -`}
                </Button>
                <Collapse in={this.state.open}>
                    <div>
                        <Card>
                            <Row className="show-grid">
                                <Col md={12}>
                                    <Form>
                                        <FormGroup controlId="formInLineName">
                                            <FormLabel>Promo Code</FormLabel>
                                            <FormControl
                                                type="text"
                                                placeholder="Enter promo code"
                                                value={this.props.promoCodeString}
                                                onChange={this.props.handleChange}
                                            />
                                        </FormGroup>
                                        <Button
                                            block={true}
                                            variant="success"
                                            className="btn-round"
                                            disabled={this.props.isDisabled}
                                            onClick={this.props.giveDiscount}
                                        >
                                            Apply
                                        </Button>
                                    </Form>
                                </Col>
                            </Row>
                        </Card>
                    </div>
                </Collapse>
            </div >

        );
    }
}

const mapStateToProps = (state: any) => ({
    promoCode: state.promoCode.value
});

export default connect (mapStateToProps, { handleChange })(PromoCodeDiscount);