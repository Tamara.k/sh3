import * as React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

interface EstimatedTotalState {

}

interface EstimatedTotalProps {
    totalSum: number;
}

class EstimatedTotal extends React.Component<EstimatedTotalProps, EstimatedTotalState> {
    render() {
        return (
            <Row>
                <Col  md={6}><h3>Price:</h3></Col>
                <Col className="total" md={6}><h3>{`$${this.props.totalSum.toFixed(2)}`}</h3></Col>
            </Row>
        );
    }
}
const mapStateToProps = (state: any) => ({
    addedItems: state.addedItems,
    total: state.total
    
});

const mapDispatchToProps = (dispatch: any) => ({
    
});
export default connect(mapStateToProps, mapDispatchToProps)(EstimatedTotal);