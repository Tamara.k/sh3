import '../App.css';
// import SubTotal from '../components/Subtotal/Subtotal';
// import PickupSavings from '../components/PickupSavings/PickupSavings';
import * as React from 'react';
import { Container } from 'react-bootstrap';
// import TaxesFees from '../components/TaxesFees/TaxesFees';
import EstimatedTotal from '../components/EstimatedTotal/EstimatedTotal';
// import ItemDetails from '../components/ItemDetails/ItemDetails';
// import PromoCodeDiscount from '../components/PromoCode/PromoCode';
import { Button } from 'react-bootstrap';
import { removeItem, updateItem } from '../actions/Cart.actions.';
import { Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { singleAddedItem } from '../reducers/cartReducer';
// import { isEmailValid } from '../utils/isEmailValid';
import Shipping from '../components/Shipping';

interface ShoppingCartState {
    total: number;
    PickupSavings: number;
    TaxesFees: number;
    disablePromoButton: boolean;
    taxes: number;
    isCardBacgroundWhite2: boolean;
    localItems: singleAddedItem[];

}
interface ShoppingCartProps {
    promoCode: string;
    HandleRemoveItem: (itemID: number, quantity: number) => void;
    HandleUpdateItem: (itemID: number, quantity: number) => void;
    EstimatedTotal: number;
    totalShipping: number;
    item: any;
}

class ShoppingCart extends React.Component<ShoppingCartProps, ShoppingCartState> {
    constructor(props: ShoppingCartProps) {
        super(props);

        this.state = {
            total: 100,
            PickupSavings: -3.85,
            TaxesFees: 0,
            disablePromoButton: false,
            taxes: 5,
            isCardBacgroundWhite2: true,
            localItems: JSON.parse(localStorage.getItem('cartItems') || '[]'),
        };
    }

    componentDidMount() {
        // const localItems = this.props.items.map((item: any) => Object.assign(item)); nuzno stob] dobavitj 
        this.setState({ localItems: this.state.localItems });
        localStorage.getItem(this.props.item);
    }

    plusQuantityDelete = (itemID: number) => {
        // const { localItems } = this.state; // destructuring
        const localItems = this.state.localItems;
        const index = localItems.findIndex(item => item.itemID === itemID);
        let clickedItem = localItems[index];
        clickedItem.quantity += 1;

        localItems[index] = clickedItem;
        this.setState({ localItems: this.state.localItems });
        this.props.HandleUpdateItem(itemID, clickedItem.quantity);

    }

    minusQuantityDelete = (itemID: number) => {
        const { localItems } = this.state; // destructuring
        // const localItems = this.state.localItems;
        const index = localItems.findIndex(item => item.itemID === itemID); //v localitems ishem item u kotoroge ego ItemId raven tomu; sto prihodit v funkciju kak parametr.
        let clickedItem = localItems[index];
        if (clickedItem.quantity >= 1) {
            clickedItem.quantity -= 1;
        }

        localItems[index] = clickedItem;
        this.setState({ localItems: this.state.localItems });
        this.props.HandleUpdateItem(itemID, clickedItem.quantity);
    }
    onHandlChangeDelete = (event: any, itemID: number) => {
        if (isNaN(event.target.value)) {
            alert('Write number!');
        } else {
            const localItems = this.state.localItems;
            const index = localItems.findIndex(item => item.itemID === itemID);
            let clickedItem = localItems[index];
            clickedItem.quantity = event.target.value;
            localItems[index] = clickedItem;
            this.setState({ localItems: this.state.localItems });
        }
    }
    
    cardColourChange = () => {
        this.setState({
            isCardBacgroundWhite2: !this.state.isCardBacgroundWhite2
        });
    }

    // componentDidMount = () => {
    //     this.setState({
    //         taxes: (this.state.total + this.state.PickupSavings) * 0.0875
    //     }, () => {
    //         this.setState({
    //             EstimatedTotal: this.props.total + this.state.PickupSavings + this.state.taxes
    //         });
    //     });
    // }
    // // HandleRemoveItem = () => {
    // //     this.props.removeItem(itemID: number);
    // // }
    // giveDiscountHandler = () => {
    //     if (this.props.promoCode === 'DISCOUNT') {
    //         this.setState({
    //             EstimatedTotal: this.props.EstimatedTotal * 0.9
    //         }, () => {
    //             this.setState({
    //                 disablePromoButton: true
    //             });
    //         }
    //         );
    //     }
    // }
    render() {
        let superTotal = this.props.EstimatedTotal + this.props.totalShipping;
        let Change = 'purchase-card';
        if (this.state.isCardBacgroundWhite2 === false) {
            Change = 'purchase-card-black';
        }
        let ChangeText = 'Change to dark';
        if (this.state.isCardBacgroundWhite2 === false) {
            ChangeText = 'Change to light';
        }

        let addedItems = JSON.parse(localStorage.getItem('cartItems') || '[]').map((item: any) => {
            let realPriceClass = '';
            if (item['discounted-price'] !== undefined) {
                realPriceClass = 'price-strike';
            }
            let discountedPriceElement = <div className='discount-price'><p>
                <b>Discounted Price: {item['discounted-price']}$</b></p></div>;
            if (item['discounted-price'] === undefined) {
                discountedPriceElement = <span />;
            }
            return (
                <div className="col-lg-4 col-md-6 mb-4" key={item.itemID}>
                    <div className={`item-desc1 ${Change}`}>
                        <div className="added-img">
                            <img src={item.image} alt={item.name} className="added-imgg" />
                        </div>
                        <span className="title">{item.name}</span>
                        <p>{item.description}</p>
                        <strong className={realPriceClass}>
                            <div className="price"> <p><b>Price: {item.price}$</b></p></div>
                        </strong>
                        <div>{discountedPriceElement}</div>
                        <p>
                            <b>Quantity:</b>
                            <span>
                                <Button className="minus" variant="info" onClick={() => this.minusQuantityDelete(item.itemID)}> - </Button>
                                <input type="text" className="quantImput" onChange={event => this.onHandlChangeDelete(event, item.itemID)} value={item.quantity} />
                                <Button className="plus" variant="info" onClick={() => this.plusQuantityDelete(item.itemID)}> + </Button>
                            </span>
                        </p>
                        <div className="add-remove">
                            <Link to="/cart" />
                            <Link to="/cart" />
                        </div>
                        <span>
                            <Button className="delete" variant="info" onClick={() => this.props.HandleRemoveItem(item.itemID, item.quantity)}>Delete item</Button>
                        </span>
                    </div>
                </div>
            );
        });
        return (
            <div className={`container`}>
                <h3>Your order</h3>
                <Row>
                    {
                        addedItems.length ? // if
                            addedItems
                            : <p>Nothing.</p>
                    }
                    {/* //addedItems if true, nothing if false */}
                </Row>
                <div>
                    <Button onClick={this.cardColourChange}>{ChangeText}</Button>
                    <Container className={'cart-summary'}>
                        {/* <SubTotal price={this.state.total.toFixed(2) viewFunction={this.listwiew}} />
                        <PickupSavings price={this.state.PickupSavings.toString()} />
                        <TaxesFees taxes={this.state.TaxesFees.toFixed(2)} /> */}
                        <hr />
                        <EstimatedTotal totalSum={this.props.EstimatedTotal} />
                        {/* <ItemDetails price={this.props.EstimatedTotal.toFixed(2)} /> */}
                        <hr />
                        <Shipping />
                        {/* <PromoCodeDiscount
                            giveDiscount={this.giveDiscountHandler}
                            isDisabled={this.state.disablePromoButton}
                            promoCodeString={this.props.promoCode}
                        /> */}
                        <hr />
                    </Container>
                    <div className="superTotal"> <h3> Total: ${superTotal}</h3> </div>
                </div>
                <hr />
                <hr />
                <hr />
                <hr />
                {/* <Button className="return" variant="info"> Return to shop</Button> */}
                <hr />
                {/* <Button className="pay" variant="info"> Pay for Your order </Button> */}
            </div>
        );
    }

}
const mapStateToProps = (state: any) => ({
    // items: state.cart.addedItems,
    EstimatedTotal: state.cart.total,
    totalShipping: state.cart.totalShipping
});

const mapDispatchToProps = (dispatch: any) => ({
    HandleRemoveItem: (itemID: number, quantity: number) => dispatch(removeItem(itemID, quantity)),
    HandleUpdateItem: (itemID: number, quantity: number) => dispatch(updateItem(itemID, quantity))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);