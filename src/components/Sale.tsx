import '../App.css';
import * as React from 'react';
import { connect } from 'react-redux';
import { Button, Row } from 'react-bootstrap';
import { handleOnUserListStyleClick } from '../actions/userPreferencesAction';

interface SaleState {
    total: number;
    PickupSavings: number;
    TaxesFees: number;
    EstimatedTotal: number;
    disablePromoButton: boolean;
    taxes: number;
    // howUserSeesItems: string;
    howUserSeesCards: string;
    isCardBacgroundWhite: boolean;
}
interface SaleProps {
    listStyle: string;
    items: any;
    handleOnUserListStyleClick: (styleName: string) => void;
}
// ///function discountPrice(item: any) {
//   return (<strong className="price-strike"> <p><b>Price: {item.price}$</b></p></strong>
//   <div className="discounted-price"><p><b>Discounted Price: {item['discounted-price']}$</b></p></div>);
// }

// //function fullPrice(item: any) {
//   return ( <p><b>Price: {item.price}$</b></p>);
// }

class Sale extends React.Component<SaleProps, SaleState> {
    constructor(props: SaleProps) {
        super(props);

        this.state = {
            total: 100,
            PickupSavings: -3.85,
            TaxesFees: 0,
            EstimatedTotal: 0,
            disablePromoButton: false,
            taxes: 5,
            // howUserSeesItems: 'grid',
            howUserSeesCards: 'white',
            isCardBacgroundWhite: true
        };
    }

    listView = () => {
        if (this.props.listStyle === 'grid') {
            this.props.handleOnUserListStyleClick('list');
        }
    }
    gridView = () => {
        if (this.props.listStyle === 'list') {
            this.props.handleOnUserListStyleClick('grid');
        }
    }
    cardColourChange = () => {
        this.setState({
            isCardBacgroundWhite: !this.state.isCardBacgroundWhite
        });
    }
    render() {
        console.log(this.state.isCardBacgroundWhite);
        let ChangeText = 'Change to dark';
        if (this.state.isCardBacgroundWhite === false) {
            ChangeText = 'Change to light';
        }
        let shopItemsWithDiscount = this.props.items.map((item: any) => {
            if (item['discounted-price'] !== undefined) {
                let realPriceClass = '';
                if (item['discounted-price'] !== undefined) {
                    realPriceClass = 'price-strike';
                }
                let discountedPriceElement = <div className='discount-price'><p>
                    <b>Discounted Price: {item['discounted-price']}$</b></p></div>;
                if (item['discounted-price'] === undefined) {
                    discountedPriceElement = <span />;
                }
                let itemsListClassName = 'col-lg-4 col-md-6 mb-4';
                if (this.props.listStyle === 'list') {
                    itemsListClassName = 'col-lg-12 mb-4';
                }

                let Change = 'card h-100';
                if (this.state.isCardBacgroundWhite === false) {
                    Change = 'card h-100 black';
                }
                return (
                    <div className={itemsListClassName} key={item.name}>
                        <div className={Change}>
                            <div className="card-image">
                                <img src={item.image} alt={item.name} />
                                <span className="card-title">{item.name}</span>
                                <span className="btn-floating halfway-fab waves-effect waves-light red" />
                            </div>
                            <div className="card-content">
                                <p>{item.description}</p>
                                <strong className={realPriceClass}>
                                    <div className="price"> <p><b>Price: {item.price}$</b></p></div>
                                </strong>
                                <div>{discountedPriceElement}</div>
                            </div><span className="addButton"> <Button variant="info">Add</Button>
                            </span>
                        </div></div>
                );
            } else {
                return null;   
            }
        });
        let amountOfItems = shopItemsWithDiscount.length;
        return (
            <React.Fragment>
                <div className="container">
                    <h3 className="center">Our items ( {amountOfItems} )  </h3>
                    <div className="row box">
                        <div id="container2">
                            <Row>
                                <div id="btnContainer">
                                    <Button className="list.btn" onClick={this.listView}> List </Button>
                                    <Button className="grid.btn" onClick={this.gridView}>Grid</Button>
                                </div>
                                <div id="btnContainer">
                                    <Button onClick={this.cardColourChange}>{ChangeText}</Button>
                                </div>
                            </Row>
                            <Row>{shopItemsWithDiscount}</Row>
                        </div>
                    </div>
                    <p className="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state: any) => ({
    items: state.cart.items,
    listStyle: state.userPreferences.listStyle
});

const mapDispatchToProps = (dispatch: any) => ({
    handleOnUserListStyleClick: (style: string) => dispatch(handleOnUserListStyleClick(style))
});

export default connect(mapStateToProps, mapDispatchToProps)(Sale);