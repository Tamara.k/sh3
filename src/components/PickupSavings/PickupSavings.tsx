import * as React from 'react';

import { Row, Col, Tooltip, OverlayTrigger } from 'react-bootstrap';

interface PickupSavingsState {
    total: number;
  }
interface PickupSavingsProps {
    price: string;
}

export default class PickupSavings extends React.Component<PickupSavingsProps, PickupSavingsState> {
    render() {
        const styles = {
            pickupSavings: {
                textDecoration: 'underline'
            },
            totalSavings: {
                color: 'red',
                fomtWeight: 800
            }
        };
        const tooltip = (
            <Tooltip id="tooltip">
                <p>Picking upo your oreder in the store helps cut costs, and we pass the savings to you.</p>
            </Tooltip>
       );
    
        return(
            <Row className="show-grid">
                <Col md={6}>
                    <OverlayTrigger placement="bottom" overlay={tooltip}>
                        <div style={styles.pickupSavings}>Pickup savings</div>
                    </OverlayTrigger>
                </Col>
                <Col style={styles.totalSavings} md={6}>
                    {`$${this.props.price}`}
                </Col>
            </Row>

        );
    }
}
