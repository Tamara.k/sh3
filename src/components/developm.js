import { h, Component, PropTypes } from 'preact';
import Card from 'preact-material-components/Card';
import TextField from 'preact-material-components/TextField';
import List from 'preact-material-components/List';
import 'preact-material-components/TextField/style.css';
import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import 'preact-material-components/List/style.css';
import axios from 'axios';
import style from './style';

//@inject('appStore') @observer
export default class Home extends Component {

  state = {
    appList: [],
    app: {
      name: '',
      description: '',
      url: '',
    },
    publicKey: 'Your generated public key goes here',
  };

  save = () => {
    const {app} = this.state;
  };

  onAppNameChange = ({target}) => this.setState({app: {...this.state.app, name: target.value}});

  onAppDescChange = ({target}) => this.setState({app: {...this.state.app, description: target.value}});

  onAppReturnURLChange = ({target}) => this.setState({app: {...this.state.app, url: target.value}});

  componentDidMount() {
    let id = this.props.id;
    this.loadApps();
    if (id && id.length > 0) {
      // axios.get('http://localhost:8081/applications').then(({data}) => this.setState({appList: data}));
    }
  }

  loadApps = () => {
    axios.get('https://apify-gw.int.kn/sc/applications').then(({data}) => this.setState({appList: data}));
  };

  createApp = (e) => {
    e.preventDefault();
    // const {appStore} = this.props;
    // appStore.createApp()
    const {app} = this.state;
    axios.post('https://apify-gw.int.kn/sc/applications', app).then(({data}) => {
      this.setState({publicKey: data});
      this.loadApps();
    });
    return false;
  };

  render() {
    const {app, appList} = this.state;
    return (
       <form class={`${style.home} page`} onSubmit={this.createApp}>
         <h1>Applications</h1>
         <div class={`${style.appsContainer}`}>
         <List>
           {appList.map(app => <div key={app} onClick={() => this.setState({app})}>
             {app.name}
           </div>)}
         </List>
         <Card class={style.card}>
           <div class={style.cardHeader}>
             {/*<h2 class=" mdc-typography--title">TLL Employee portal</h2>*/}
             {/*<div class=" mdc-typography--caption">Portal for Tallinn employees</div>*/}
           </div>
           <div class={style.cardBody}>
             <TextField value={app.name}
                        onChange={this.onAppNameChange}
                        helperText="Type your application name"
                        helperTextPersistent={true} fullwidth
                        dense/>
             <TextField value={app.description}
                        onChange={this.onAppDescChange}
                        helperText="Type your application description"
                        helperTextPersistent={true}
                        fullwidth dense/>
             <TextField value={app.url}
                        onChange={this.onAppReturnURLChange}
                        helperText="Type your application return url"
                        helperTextPersistent={true}
                        fullwidth dense/>
             <div class={style.code}>
               {this.state.app.publicKey || this.state.publicKey}
             </div>
           </div>
           <Card.Actions class={style.actions}>
             <Card.ActionButton type="submit" disabled={app.key}>SAVE</Card.ActionButton>
           </Card.Actions>
         </Card>

           <div className={style.description}>
             <ol>
               <li>User clicks SSO login on your app</li>
               <li>User is redirected to https://apify.int.kn/auth/login</li>
               <li>Apify login's user via SSO</li>
               <li>Apify returns user to <b>{app.url}?token=[HERE IS YOUR JWT TOKEN]</b></li>
               <li>User sends JWT with any authenticated request to your app</li>
               <li>App validates JWT using public key</li>
               <li>App gets user data from JWT using public key</li>
             </ol>
           </div>
         </div>
       </form>
    );
  }
}

Home.propTypes = {
  
}