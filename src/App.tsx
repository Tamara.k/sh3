import './App.css';
import * as React from 'react';
// import handleChange from './actions/promoCodeActions';
import { Route, Switch, Link, withRouter} from 'react-router-dom';
// import Image from 'react-bootstrap/Image';
import Home from './pages/Home';
import Store from './components/Store';
// import itemPage from './pages/ItemPage';
import { connect } from 'react-redux';
import ShoppingCart from './components/ShoppingCart';
import { Navbar } from 'react-bootstrap';
import Sale from './components/Sale';
import { fetchItem } from './actions/Cart.actions.';
import { fetchUser } from './actions/UserActions';
import ItemDescr from './pages/ItemDescr';
import Login from './components/Login';
import UserDetails from './pages/UserDetails';
// import { browserHistory} from 'react-router';


interface AppState {
  total: number;
  PickupSavings: number;
  TaxesFees: number;
  EstimatedTotal: number;
  disablePromoButton: boolean;
  taxes: number;
  isLinkActive5: boolean;
  menu: string;
}
interface AppProps {
  promoCode: string;
  fetchItemFunction: () => void;
  fetchUser: () => void;
  // name: string;
}

class App extends React.Component<AppProps, AppState> {
  constructor(props: any) {
    super(props);

    this.state = {
      total: 100,
      PickupSavings: -3.85,
      TaxesFees: 0,
      EstimatedTotal: 0,
      disablePromoButton: false,
      taxes: 5,
      isLinkActive5: false,
      menu: 'home',
    };
  }
  componentDidMount() {
    this.props.fetchItemFunction();
    // this.props.fetchUser();
    
  }

  activeLink = (menuString: string) => {
    this.setState({
      menu: menuString
    });
  }

  render() {
    let homeActiveOrNot = '';
    if (this.state.menu === 'home') {
      homeActiveOrNot = 'homeactive';
    }
    let ShopActiveOrNot = '';
    if (this.state.menu === 'shop') {
      ShopActiveOrNot = 'shopactive';
    }
    let ShCartActiveOrNot = '';
    if (this.state.menu === 'shopping-cart') {
      ShCartActiveOrNot = 'shCartactive';
    }
    let SaleActiveOrNot = '';
    if (this.state.menu === 'sale') {
      SaleActiveOrNot = 'saleactive';
    }
    let LoginActiveOrNot = '';
    if (this.state.isLinkActive5 === true) {
      LoginActiveOrNot = 'loginactive';
    }
    return (
      <div>
        <div className="cover">
          <header>
            <Navbar>
              <ul>
              <li className={homeActiveOrNot}>
            <Link to={'/'} onClick={() => this.activeLink('home')}>Home</Link>
            </li>
                <li className={ShopActiveOrNot}>
                  <Link to={'/shop'} onClick={() => this.activeLink('shop')}>Shop</Link>
                </li>
                <li className={ShCartActiveOrNot} >
                  <Link to={'/cart'} onClick={() => this.activeLink('shopping-cart')}>Shopping cart</Link>
                </li>
                <li className={SaleActiveOrNot}>
                 <Link to={'/sale'} onClick={() => this.activeLink('sale')}>Sale</Link></li>
                <li className={LoginActiveOrNot}>
                  <Link to={'/login'} className="btn" title="Register / Log In" onClick={() => this.activeLink('login')}>Register/Log In</Link>
                </li>
              </ul></Navbar>
          </header>
          <div className="App">
          {/* <Router history={browserHistory}>  */}
            <Switch>
              
              <Route exact={true} path="/" component={Home} />
              <Route exact={true} path="/shop" component={Store} />
              <Route path="/cart" component={ShoppingCart} />
              {/* <Route path="/item" component={itemPage} /> */}
              <Route path="/sale" component={Sale} />
              <Route exact={true} path="/shop/:id" component={ItemDescr} />  
              <Route exact={true} path="/login" component={Login} />
              <Route exact={true} path="/users/:id" component={UserDetails} /> 
              {/* or:pathParam?? */}
            
            </Switch> 
        {/* </Router> */}
          </div>
        </div>
      </div> 
    );
  }
}

const mapStateToProps = (state: any) => ({
  promoCode: state.promoCode.value
});
const mapDispatchToProps = (dispatch: any) => ({
  fetchItemFunction: () => dispatch(fetchItem()),
  fetchUser: () => dispatch(fetchUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
