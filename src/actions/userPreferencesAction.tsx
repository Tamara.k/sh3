import { CHANGE_LIST_STYLE } from './types';

export const handleOnUserListStyleClick = (styleName: string) => {
    return{
        type: CHANGE_LIST_STYLE,
        styleName
    };
};