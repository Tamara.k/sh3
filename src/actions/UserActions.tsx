import { FETCH_USER } from './cart';

export const fetchUser = () => {
    return (dispatch: any) => {
        fetch('http://localhost:3030/user')
            .then(response => response.json())
            .then(data => {
                dispatch({
                        type: FETCH_USER,
                        user: data,
                    }
                );
            });
        };
  };
