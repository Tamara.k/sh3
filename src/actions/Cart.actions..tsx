import { ADD_TO_CART, REMOVE_ITEM, SUB_QUANTITY, ADD_QUANTITY, UPDATE_ITEM, FETCH_ITEMS } from './cart';

export const addToCar = (id: any , quantity: number) => {
    return{
        type: ADD_TO_CART,
        id,
        quantity: quantity
    };
};
export const removeItem = (id: any, quantity: number) => {
    return{
        type: REMOVE_ITEM,
        id,
        quantity: quantity
    };
};
export const subtractQuantity = (id: any) => {
    return{
        type: SUB_QUANTITY,
        id
    };
};
export const addQuantity = (id: any) => {
    return{
        type: ADD_QUANTITY,
        id
    };
};
export const updateItem = (id: any, quantity: number) => {
    return{
        type: UPDATE_ITEM,
        id,
        quantity: quantity
    };
};

export const fetchItem = () => {
    return (dispatch: any) => {
        fetch('http://localhost:3030/shop-items')
            .then(response => response.json())
            .then(data => {
                dispatch({
                        type: FETCH_ITEMS,
                        items: data
                    }
                );
            });
        };
  };
 