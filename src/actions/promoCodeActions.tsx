import { PROMO_CODE } from './types';

const handleChange = (e: any) => (dispatch: any)  => {
    dispatch({
        type: PROMO_CODE,
        payload: e.target.value
    });
};

export default handleChange;