import { combineReducers } from 'redux';
import promoCodeReducer from './promoCodeReducer';
import CartReducer from './cartReducer';
import UserReducer from './UserReducer';
import userPreferencesReducer from './userPreferencesReducer';
export default combineReducers({
    promoCode: promoCodeReducer,
    cart: CartReducer,
    userPreferences: userPreferencesReducer,
    users: UserReducer
});
