
import { ADD_TO_CART, REMOVE_ITEM, UPDATE_ITEM, FETCH_ITEMS } from '../actions/cart';
// import ItemDetails from 'src/components/ItemDetails/ItemDetails';
// import { string, any } from 'prop-types';

// function add(state: any = initialState, action: any): any {
// switch (action.type) {
//   case ItemDetails:
//     return {
//      ...state,
//      value: action.payload
//    };
//  default:
//    return state;
//  }
// }
export interface singleAddedItem {
  name: string;
  price: number;
  'discounted-price': number;
  image: string;
  description: string;
  quantity: number;
  itemID: number;
}

interface item {
  name: string;
  price: number;
  'discounted-price': number;
  image: string;
  description: string;
  itemID: number;
}

const initialState = {
  items: [] as item[],
  addedItems: [] as singleAddedItem[], //pustoi massiv kotorõi ozidaet, sto v nego dobavjaty singleadded item.
  total: 0,
  totalShipping: 0,
  value: 0,
  storage:0,
};
// const shippingCost = (quantity: number) => {
//   let differentShippingCost = 0;
//   if (quantity >= 2 ) {
//       differentShippingCost = 10;
//   } else if (quantity <= 2 ) {
//       differentShippingCost = 20;
//   } else {
//       differentShippingCost = 0;
//   }
//   return quantity * differentShippingCost;   ///pribavljajet predidushii shipping cost 
// }

const totalShippingCost = (items: singleAddedItem[]) => {
  let totalShipping = 0;
  let specialShippingPrice = 0;
  if (items.length >= 5) {
    specialShippingPrice = 15;
  }

  items.map((item: any) => {
      let differentShippingCost = 0;
      if (item.quantity >= 2 ) {
          differentShippingCost = 10;
      } else if (item.quantity < 2 ) {
          differentShippingCost = 20;
          if (specialShippingPrice !== 0) {
            differentShippingCost = specialShippingPrice;
          }
      } else {differentShippingCost = 0; }
      totalShipping = totalShipping +  item.quantity * differentShippingCost;   // pribavljajet predidushii shipping cost 
  });
  return totalShipping;
};
const CartReducer = (state = initialState, action: any) => {
  if (action.type === ADD_TO_CART) {
    const addedItems = JSON.parse(localStorage.getItem('cartItems') || '[]');
    let addedItem = state.items.find(item => item.itemID === action.id);
    // check if the action id exists in the addedItems
    let existed_item = addedItems.find((item: any) => action.id === item.itemID);
    if (existed_item) { // item is already in the basket
      existed_item.quantity += action.quantity; // I want to add the same item, I increase the amount by one
      localStorage.setItem('cartItems', JSON.stringify(addedItems));
      return {
        ...state,
        total: state.total + existed_item.price * action.quantity,
        totalShipping: totalShippingCost([...state.addedItems]) ,
        //  localStorage.setItem(...state, total, totalShipping); 
      };
    } else { // completely new item
      const itemToAdd = { ...addedItem } as singleAddedItem; // make a copy, with the same structure like existed_item
      itemToAdd.quantity = action.quantity; // set quantity to 1
      // calculating the total
      let itemRealPrice = itemToAdd.price;
      if (itemToAdd['discounted-price'] !== undefined) {
        itemRealPrice = itemToAdd['discounted-price'];
      }
      let newTotal = state.total + itemRealPrice * action.quantity; // calculate new price

      localStorage.setItem('cartItems', JSON.stringify([...addedItems, itemToAdd]));
      return {
        ...state,
        addedItems: [...state.addedItems, itemToAdd],
        total: newTotal,
        totalShipping: totalShippingCost([...state.addedItems, itemToAdd])
      };

    }
  } else if (action.type === REMOVE_ITEM) {
    const addedItems = JSON.parse(localStorage.getItem('cartItems') || '[]');
    let removedItem = addedItems.find((item: any) => item.itemID === action.id) as singleAddedItem; // iz etogo massiva nahodim objekt i on budet to4no takoi kak single.addeditem
    let new_items = addedItems.filter((item: any) => action.id !== item.itemID); // v etom massive vse itemõ na kotorõe 4el ne nazal
    // let removedItemQuantity = state.addedItems.filter(quantity => quantity.itemID === item.quantity)
    
    // calculating the total
    let removedItemRealPrice = removedItem.price;
    if (removedItem['discounted-price'] !== undefined) {
      removedItemRealPrice = removedItem['discounted-price'];
    }
    let newTotal = (state.total - (removedItemRealPrice * removedItem.quantity));
    localStorage.setItem('cartItems', JSON.stringify(new_items));
    return {
      ...state,
      addedItems: new_items,
      total: newTotal,
      totalShipping: totalShippingCost(new_items)
    };
  } else if (action.type === UPDATE_ITEM) {
    const addedItems = JSON.parse(localStorage.getItem('cartItems') || '[]');
    let removedItem = addedItems.find((item: any) => item.itemID === action.id) as singleAddedItem;
    if (removedItem.quantity !== action.quantity) {
      removedItem.quantity = action.quantity;
    }
    let lastItemsum = 0;
    addedItems.map((item: any) => {
      let removedItemRealPrice = item.price;
      if (item['discounted-price'] !== undefined) {
        removedItemRealPrice = item['discounted-price'];
      }
    
      lastItemsum = lastItemsum + (removedItemRealPrice * item.quantity); //  bõlo 0, slalo 0 +novoe dobavlennoe ili izmenennoe
    });
    localStorage.setItem('cartItems', JSON.stringify(addedItems));
    // let newTotal =
    console.log(state.addedItems);
    return {
      ...state,
      total: lastItemsum,
      totalShipping: totalShippingCost(state.addedItems)
    };
  } else if (action.type === FETCH_ITEMS) {
    return {
      ...state,
      items: action.items
    };
  } else {
    return state; // vozvrashaet state, neizmenennõm.
  }

};

export default CartReducer;