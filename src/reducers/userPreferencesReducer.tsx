import { CHANGE_LIST_STYLE } from 'src/actions/types';

const initialState = {
    listStyle: 'grid'
};

export default function(state: any = initialState, action: any) {
  switch (action.type) {
    case CHANGE_LIST_STYLE:
      return {
        ...state,
        listStyle: action.styleName
      };
    default:
      return state;
  }
}