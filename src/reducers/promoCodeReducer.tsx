import { PROMO_CODE } from 'src/actions/types';

const initialState = {
    open: false,
    value: ''
};

export default function(state: any = initialState, action: any) {
  switch (action.type) {
    case PROMO_CODE:
      return {
        ...state,
        value: action.payload
      };
    default:
      return state;
  }
}