import { FETCH_USER } from '../actions/cart';

// const initialUserState {
//    return state;
// }
export interface User1 {
    name: string;
    userName: string;
    isAdmin: boolean;
}

const InitialState = {
    user: {} as User1,
    userWelcome: 'g'
};
const UserReducer = (state = InitialState, action: any) => {
    if ( action.type === FETCH_USER ) {

        console.log(state.user.name);
        let userName1 = action.user.name; 
        //
        let userWelcome = 'g';
        userWelcome = 'Welcome ' + userName1 + ' !';
        return {
            ...state,
            user: action.user, // EXPLAIN!
            userWelcome: userWelcome
        }; 
    } else {
        return state;
      }
};

export default UserReducer;
