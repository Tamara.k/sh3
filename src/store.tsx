import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
const initialState = {
};

const middleware = [ReduxThunk];

// const store = createStore(
//     rootReducer,
//     initialState,
//     compose(
//         applyMiddleware(...middleware)
//         // window.__REDUX_DEVTOOLS_EXTENSION__ ? window.devTooldExtension() : f => f
//     )
// );

const store = createStore(rootReducer, initialState, composeWithDevTools(
    applyMiddleware(...middleware)));

export default store;