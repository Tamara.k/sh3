import * as React from 'react';
import '../App.css';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { removeItem, updateItem } from '../actions/Cart.actions.';
import { addToCar } from '../actions/Cart.actions.';

export interface Item {
    name: string;
    price: number;
    'discounted-price': number;
    image: string;
    description: string;
    quantity: number;
    itemID: number;
}
interface ItemDescrState {
    localItem: any;
    locationChanged: boolean;
}
interface ItemDescrProps {
    addToCarFunction: (itemID: number, quantity: number) => void;
    match: any;
    location: any;
    history: any;

}

class ItemDescr extends React.Component<ItemDescrProps, ItemDescrState> {

    constructor(props: ItemDescrProps) {
        super(props);

        this.state = {
            localItem: {} as Item,
            locationChanged: false
        };
    }
    componentDidMount() {
        const { id } = this.props.match.params;
        console.log(this.props);
        fetch(`http://localhost:3030/shop-items/${id}`)
            .then(response => response.json())
            .then((data) => {
                const ItemWithQuantity = Object.assign(data, { quantity: 1 });
                this.setState({ localItem: ItemWithQuantity });
            });
            if (this.props.history.length > 2) {
                this.setState({ locationChanged: true });
            }
    }
    // componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
    //     if (prevProps.history.location !== this.props.history.location) {
    //         this.setState({ locationChanged: true });
    //     }
    //}
    plusQuantity = (itemID: number) => {
        const localItem = this.state.localItem;
        localItem.quantity += 1; // item.quantity??
        this.setState({ localItem });
    }

    minusQuantity = (itemID: number) => {
        const localItem = this.state.localItem;
        if (localItem.quantity >= 1) {
            localItem.quantity -= 1;
        }
        this.setState({ localItem });
    }
    onHandlChange = (event: any, itemID: number) => {
        if (isNaN(event.target.value)) {
            alert('Write number!');
        } else {
            const localItem = this.state.localItem;
            localItem.quantity = event.target.value;
            this.setState({ localItem });
        }
    }

    render() {
        let realPriceClass = '';
        const localItem = this.state.localItem;
        if (localItem['discounted-price'] !== undefined) {
            realPriceClass = 'price-strike';
        }
        let discountedPriceElement = <div className='discount-price'><p>
            <b>Discounted Price: {localItem['discounted-price']}$</b></p></div>;
        if (localItem['discounted-price'] === undefined) {
            discountedPriceElement = <span />;
        }
        let locationCh = <div />;
        if (this.state.locationChanged === true) {
            locationCh = <Button variant="info" href="http://localhost:3000/shop"> Go back </Button>
        }
        console.log(this.props.history.length);
        return (
            <div>
                <div className="ItemDescript" key={localItem.itemID}>
                    <div className="card-image">
                        <img src={localItem.image} alt={localItem.name} className="img-fluid" />
                        <span className="card-title">{localItem.name}</span>
                        <span className="btn-floating halfway-fab waves-effect waves-light red" />
                    </div>
                    <div className="card-content">
                        <p className="single-item-description">{localItem.description}</p>
                        <strong className={realPriceClass}>
                            <div className="price"> <p><b>Price: {localItem.price}$</b></p></div>
                        </strong>
                        {/* //skobki stobõ pokazattj znacenie peremennoi v html */}
                        <div>{discountedPriceElement}</div>
                    </div>  <Button
                        variant="info"
                        onClick={() => this.props.addToCarFunction(localItem.itemID, localItem.quantity)}
                        className="add-to-cart"
                    >
                        Add
              </Button>
                    <Button className="minus" variant="info" onClick={() => this.minusQuantity(localItem.itemID)}> - </Button>
                    <input type="text" className="quantImput" onChange={event => this.onHandlChange(event, localItem.itemID)} value={localItem.quantity} />
                    <Button className="plus" variant="info" onClick={() => this.plusQuantity(localItem.itemID)}> + </Button>
                    <div> {locationCh}</div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state: any) => ({
    items: state.cart.items,
    listStyle: state.userPreferences.listStyle
});

const mapDispatchToProps = (dispatch: any) => ({
    addToCarFunction: (itemID: number, quantity: number) => dispatch(addToCar(itemID, quantity)),
    HandleRemoveItem: (itemID: number, quantity: number) => dispatch(removeItem(itemID, quantity)),
    HandleUpdateItem: (itemID: number, quantity: number) => dispatch(updateItem(itemID, quantity))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDescr);
