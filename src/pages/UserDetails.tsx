import * as React from 'react';
import { FaRegCircle } from 'react-icons/fa';

export interface User {
    name: any;
    userName: any;
    isAdmin: boolean;
    isOnline: boolean;
    id: number;
}
interface UserDetailsProps {
    match: any;
}
interface UserDetailsState {
    LocalUser: any;
}
class UserDetails extends React.Component<UserDetailsProps, UserDetailsState> {
    constructor(props: UserDetailsProps) {
        super(props);

        this.state = {
            LocalUser: [] as User[],
        };
    }
    componentDidMount() {
        const { id } = this.props.match.params;
        fetch(`http://localhost:3030/users/${id}`)
            .then(response => response.json())
            .then((data) => {
                this.setState({ LocalUser: data });
                console.log(this.state);
            });
    }
    IsLoggedIn = (LocalUser: any) => {

        let circle;
        if (LocalUser.isOnline === true) {

            circle = "logedInCircle";

        } else {
            circle = "NotlogedInCircle"
        }
        return circle;
    }
    render() {
        console.log(this.state.LocalUser.isOnline);
        return (
            <div className="UserDet" key={this.state.LocalUser.id}>
                <div className="Name"><p>Name: {this.state.LocalUser.name}</p>
                </div>
                <div className="UserName"><p>UserName: {this.state.LocalUser.userName}</p>
                </div>

                <div className="isAdmin"><p>Admin rights: {String(this.state.LocalUser.isAdmin)}</p>
                </div>
                <div className="isOnline"><p>{this.state.LocalUser.isOnline}<FaRegCircle className={this.IsLoggedIn(this.state.LocalUser)} /></p>
                </div>
            </div>
        );    
    }
}
export default UserDetails;

    // "name": "Helena Lissenko",
    //     "userName": "Helena.L",
    //     "isAdmin": true,
    //     "isOnline": true