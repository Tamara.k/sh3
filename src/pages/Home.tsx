import '../App.css';
import * as React from 'react';
//import { Container } from 'react-bootstrap';
import { connect } from 'react-redux';
// import handleChange from '../actions/promoCodeActions';
// import Image from 'react-bootstrap/Image';
import { fetchUser } from '../actions/UserActions';
import { FaGift, FaRegCircle } from 'react-icons/fa';

interface HomeState {
    total: number;
    PickupSavings: number;
    TaxesFees: number;
    EstimatedTotal: number;
    disablePromoButton: boolean;
    taxes: number;
    data: any;
    Users: any;
}
interface HomeProps {
    promoCode: string;
    userWelcome: any;
    fetchUser: () => void;
}

class Home extends React.Component<HomeProps, HomeState> {
    constructor(props: HomeProps) {
        super(props);

        this.state = {
            total: 100,
            PickupSavings: -3.85,
            TaxesFees: 0,
            EstimatedTotal: 0,
            disablePromoButton: false,
            taxes: 5,
            data: [],
            Users: [],
            // userWelcome: this. props.userWelcome
        };
    }
    componentDidMount() {
        this.props.fetchUser();
        fetch(`http://localhost:3030/users`)
            .then(response => response.json())
            .then((data) => {
                this.setState({ Users: data });
            });
    }
    IsLoggedIn = (item: any) => {

        let circle;
        if (item.isOnline === true) {

            circle = "logedInCircle";

        } else {
            circle = "NotlogedInCircle"
        }
        return circle;
    }
    render() {

        //    var NotLoggedUser = this.state.Users.filter((NotLoggedUser1: any) => NotLoggedUser1.isOnline === false);

        console.log(this.state.Users)
        console.log('home');
        return (
            <div className="container wel">
                <form className="flex-form">
                    <label className="label">
                        <FaGift className="fagift" />
                    </label>
                    <input type="search" placeholder="What do You want to bye ?" />
                    <input type="submit" value="Search" />
                </form>
                <h3 className="welcom">{this.props.userWelcome}</h3>
                <div className="loggedUsers">
                    {
                        this.state.Users.map((item: any) =>
                            <a className="station" key={item.userName} href={`http://localhost:3000/users/${item.id}`}>
                                <span><FaRegCircle className={this.IsLoggedIn(item)} /></span>
                                {item.userName}
                            </a>
                        )
                    }
                </div>

                {/* <div className="NotloggedUsers"> {NotLoggedUser.map((item:any) => <div className="station" key={item.userName}><FaRegCircle className="NotlogedInCircle" />{item.userName}</div>)}  </div> */}
            </div>);
    }
}

const mapStateToProps = (state: any) => ({
    promoCode: state.promoCode.value,
    userWelcome: state.users.userWelcome //iz redux state
});
const mapDispatchToProps = (dispatch: any) => ({
    fetchUser: () => dispatch(fetchUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
