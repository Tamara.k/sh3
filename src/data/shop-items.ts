export const shopItems = [
{
'name': 'Computer Desk',
'description': 'Ameriwood Home Haven Retro Desk with Riser, Espresso',
'image': 'https://images-na.ssl-images-amazon.com/images/I/81QbP2IuKRL._SL1500_.jpg',
'price': 73,
  'discounted-price': 53,
  'itemID': 1
  },
  {
    'name': 'Desk lamp',
	'description': 'Swing Arm Lamp, LED Desk Lamp with Clamp, 9W Eye-Care Dimmable Light, Timer, Memory,6 Color Modes, JolyJoy Modern Architect Table Lamp for Task Study Reading Working/Home Dorm Office (Black)',
    'image': 'https://images-na.ssl-images-amazon.com/images/I/61M3PHnEnnL._SL1200_.jpg',
    'price': 59.99,
    'discounted-price': 48.99,
    'itemID': 2
  },
  {
    'name': 'Executive Office Chair',
    'description': 'Smugdesk High Back Executive Office Chair with Thick Padding Headrest and Armrest Home Office Chair with Tilt Function, Black',
    'image': 'https://images-na.ssl-images-amazon.com/images/I/61oNKSNDVDL._SL1001_.jpg',
    'price': 269.99,
    'itemID': 3
  },
  {
    'name': 'Desk Organizer',
    'description': 'SimpleHouseware Mesh Desk Organizer with Sliding Drawer, Double Tray and 5 Upright Sections, Black',
    'image': 'https://images-na.ssl-images-amazon.com/images/I/A1SjZjhQ9VL._SL1500_.jpg',
    'price': 24.87,
    'itemID': 4
  },
  {
    'name': 'Newtons Cradle',
    'description': 'Toys for Desk, Newtons Cradle Magnetic Balls for Adults Stress Relief, Cool Fun Office Games Desktop Accessories,Calm Down Fidgets Kit Avoid Anxiety, Small Sensory Kids Toy',
    'image': 'https://images-na.ssl-images-amazon.com/images/I/51vjUuPCheL._SL1000_.jpg',
    'price': 19.99,
    'discounted-price': 17,
    'itemID': 5
  },
  {
    'name': 'Wall art',
    'description': 'Baisuwallart-Brooklyn Bridge New York City Night Prints Canvas Wall Art 1 Piece Paintings Pictures Artwork Ready to Hang for Home Decoration Office Wall Décor',
    'image': 'https://images-na.ssl-images-amazon.com/images/I/61B-lNfZaAL._SL1200_.jpg',
    'price': 45.90,
    'discounted-price': 40,
    'itemID': 6
  }
];